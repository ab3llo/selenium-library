﻿using System;
using OpenQA.Selenium;
using SeleniumLibrary.Core;
using SeleniumLibrary.Enums;

namespace SeleniumLibrary.Tests
{
    public class SearchPage : BasePage
    {
        private readonly string searchInputSelector="#tsf > div:nth-child(2) > div > div.RNNXgb > div > div.a4bIc > input";
        private readonly string welcomeMessageSelector = "//*[@id='gbw']/div/div/div[2]/div[4]/div/div/div[2]/div/a[1]";


        public SearchPage(SeleniumDriver seleniumDriver) : base(seleniumDriver)
        {

        }

        public void GoToGoogleHomePage()
        {
            _seleniumDriver.GoToUrl("http://www.google.co.uk");
        }

        public void CloseGoogleWelcomeMessage(Browsers browser)
        {
            switch (browser)
            {
                case Browsers.Chrome:

                    if (_seleniumDriver.ElementExist(By.XPath(welcomeMessageSelector)))
                    {
                        _seleniumDriver.FindElement(By.XPath(welcomeMessageSelector)).Click();
                    }
                    return;
            }
        }

        public void Search(string searchQuery)
        {
            IWebElement searchInputField = _seleniumDriver.FindElement(By.CssSelector(searchInputSelector));

            searchInputField.Clear();
            searchInputField.SendKeys(searchQuery);
            searchInputField.SendKeys(Keys.Enter);
        }
    }
}
