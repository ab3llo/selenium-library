﻿using System;
using OpenQA.Selenium;
using SeleniumLibrary.Core;
using SeleniumLibrary.Enums;
using Xunit;

namespace SeleniumLibrary.Tests
{
    public class SearchPageTests : IDisposable
	{
		private SeleniumDriver _seleniumDriver;
        private IWebDriver _driver = null;

        public SearchPageTests()
		{
            _seleniumDriver = new SeleniumDriver(_driver,Browsers.Chrome);
		}

		[Fact]
		public void GoToGoogleHomePage()
		{
			SearchPage searchPage = new SearchPage(_seleniumDriver);

            searchPage.GoToGoogleHomePage();
            searchPage.CloseGoogleWelcomeMessage(Browsers.Chrome);
            Assert.Equal("https://www.google.co.uk/?gws_rd=ssl", _seleniumDriver.Url);
		}

        [Theory]
        [InlineData("Cars")]
        [InlineData("Nike")]
        [InlineData("NBA")]
        [InlineData("NFL")]
        [InlineData("Manchester United")]
        public void EnterQueryAndSearchOnGoogle(string query)
        {
            SearchPage searchPage = new SearchPage(_seleniumDriver);

            searchPage.GoToGoogleHomePage();
            searchPage.CloseGoogleWelcomeMessage(Browsers.Chrome);

            Assert.Equal("https://www.google.co.uk/?gws_rd=ssl", _seleniumDriver.Url);

            searchPage.Search(query);
            string pageUrl = _seleniumDriver.Url;
            string resultUrl = "https://www.google.co.uk/search?";
            Assert.Contains(resultUrl, pageUrl);
        }

		public void Dispose()
		{
            _seleniumDriver.GetBrowser().Quit();
		}
	}
}
