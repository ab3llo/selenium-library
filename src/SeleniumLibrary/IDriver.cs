﻿

namespace SeleniumLibrary.Core
{
	public interface IDriver : IElementFinder, INavigation, IBrowser
	{
	}
}