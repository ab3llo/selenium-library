﻿using System;
using OpenQA.Selenium;
using SeleniumLibrary.Enums;

namespace SeleniumLibrary.Core
{
	public partial class SeleniumDriver : IBrowser
	{

		public void ClickBackButton()
		{
			_driver.Navigate().Back();
		}

		public void ClickForwardButton()
		{
			_driver.Navigate().Forward();
		}

		public void ClickRefresh()
		{
			_driver.Navigate().Refresh();
		}

		public void LaunchNewBrowser()
		{
			throw new NotImplementedException();
		}

		public void MaximizeBrowserWindow()
		{
			_driver.Manage().Window.Maximize();
		}

		public void Quit()
		{
			_driver.Close();
		}

        public IWebElement SwitchToActiveElement()
        {
            return _driver.SwitchTo().ActiveElement();
        }

        public void SwitchToAlert()
        {
            _driver.SwitchTo().Alert();
        }

		public void SwitchToDefault()
		{
			_driver.SwitchTo().DefaultContent();
		}


        public void SwitchToParentFrame()
        {
            _driver.SwitchTo().ParentFrame();
        }
		public void SwitchToFrame(string name)
		{
			_driver.SwitchTo().Frame(name);
		}

		public void WaitForAjax()
		{
			throw new NotImplementedException();
		}

		public void WaitUntilReady()
		{
			throw new NotImplementedException();
		}
	}
}
