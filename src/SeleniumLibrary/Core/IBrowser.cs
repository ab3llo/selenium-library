﻿using System;
namespace SeleniumLibrary.Core
{
	public interface IBrowser
	{
		void SwitchToFrame(string name);

		void SwitchToDefault();

		void Quit();

		void ClickBackButton();

		void ClickForwardButton();

		void ClickRefresh();

		void MaximizeBrowserWindow();

		void LaunchNewBrowser();

		void WaitUntilReady();

		void WaitForAjax();
	}
}
