﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace SeleniumLibrary.Core
{
	public interface IElementFinder
	{
		IWebElement FindElement(By by);

		IEnumerable<IWebElement> FindElements(By by);

		bool IsElementPresent(By by);
	}
}
