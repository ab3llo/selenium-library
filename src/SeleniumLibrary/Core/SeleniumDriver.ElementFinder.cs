﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace SeleniumLibrary.Core
{
    public partial class SeleniumDriver : IElementFinder
    {
        public IWebElement FindElement(By by)
        {
            return _driver.FindElement(by);
        }

        public IEnumerable<IWebElement> FindElements(By by)
        {
            return _driver.FindElements(by);
        }

        public bool IsElementPresent(By by)
        {
            return _driver.FindElement(by).Displayed;
        }

        public bool ElementExist(By by)
        {
            try
            {
                _driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }
}
