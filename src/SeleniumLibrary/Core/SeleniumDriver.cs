﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Safari;
using SeleniumLibrary.Config;
using SeleniumLibrary.Enums;

namespace SeleniumLibrary.Core
{
    public partial class SeleniumDriver : IDriver
	{
		IWebDriver _driver;
		DriverOptions _options;

		public IWebDriver GetBrowser() => _driver;

		public SeleniumDriver(IWebDriver driver, Browsers type)
		{
			_driver = driver;
			ResolveBrowser(type);
		}

		private void ResolveBrowser(Browsers type)
		{
            string hubUri = ConfigurationHelper.Get("selenium:hubUri");
			switch (type)
			{
				case Browsers.Chrome:
					_options = new ChromeOptions();
                    ChromeOptions chromeOptions = new ChromeOptions();

                    chromeOptions = (ChromeOptions) _options;
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");

					_driver = new RemoteWebDriver(new Uri(hubUri),chromeOptions);
					break;
				case Browsers.Firefox:
					_options = new FirefoxOptions();
					_driver = new RemoteWebDriver(_options);
					break;
				case Browsers.IE11:
					_options = new InternetExplorerOptions();
					_driver = new RemoteWebDriver(_options);
					break;
				case Browsers.Safari:
					_options = new SafariOptions();
					_driver = new RemoteWebDriver(_options);
					break;
			}

		}
	}
}
