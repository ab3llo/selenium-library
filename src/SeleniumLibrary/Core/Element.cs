﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace SeleniumLibrary
{
    public class Element : IElement
	{
		protected readonly IWebElement _webElement;

		protected readonly IWebDriver _driver;


		public Element(IWebDriver driver, IWebElement webElement)
		{
			_driver = driver;
			_webElement = webElement;
		}

		public bool IsVisible => _webElement.Displayed;

		public int Width => _webElement.Size.Width;

		public string CssClass => _webElement.GetAttribute("className");

		public string Content => _webElement.Text;

		public void Click()
		{
			_webElement.Click();
		}

		public IWebElement FindElement(By by)
		{
			return _driver.FindElement(by);
		}

		public IEnumerable<IWebElement> FindElements(By by)
		{
			return _driver.FindElements(by);
		}

		public string GetAttribute(string name)
		{
			return _webElement.GetAttribute(name);
		}

		public bool IsElementPresent(By by)
		{
			return _driver.FindElement(by).Displayed;
		}

		public void MouseClick()
		{
			Actions builder = new Actions(_driver);
			builder.MoveToElement(_webElement).Click().Build().Perform();
		}

		public void WaitForExists()
		{
			throw new NotImplementedException();
		}

		public void WaitForNotExists()
		{
			throw new NotImplementedException();
		}
	}
}
