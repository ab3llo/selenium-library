﻿using System;
using System.Web;
using OpenQA.Selenium.Support.UI;
using SeleniumLibrary.Events;

namespace SeleniumLibrary.Core
{
	public partial class SeleniumDriver : INavigation
	{

		public string Url => _driver.Url;

		public string Title => _driver.Title;

		public event EventHandler<PageEventArgs> Navigated;

		public void Navigate(string relativeUrl, string currentLocation, bool sslEnabled = false)
		{
			throw new NotImplementedException();
		}

		public void Navigate(string currentLocation, bool sslEnabled = false)
		{
			throw new NotImplementedException();
		}

		public void NavigateByAbsoluteUrl(string absoluteUrl, bool useDecodedUrl = true)
		{
			string urlToNavigateTo = absoluteUrl;

			if(useDecodedUrl)
			{
				urlToNavigateTo = HttpUtility.UrlDecode(urlToNavigateTo);
			}

			_driver.Navigate().GoToUrl(urlToNavigateTo);
		}

		public void GoToUrl(string url)
		{
			_driver.Navigate().GoToUrl(url);
		}

		public void WaitForPartialUrl(string url)
		{
			WebDriverWait wait = new WebDriverWait (_driver,TimeSpan.FromSeconds(60))
			{
				PollingInterval = TimeSpan.FromSeconds(0.3)
			};

			wait.Until(x => x.Url.Contains(url) == true);
			RaiseNavigated(_driver.Url);
			_driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
		}

		private void RaiseNavigated(string url)
		{
			Navigated?.Invoke(this, new PageEventArgs(url));
		}

		public void WaitForUrl(string url)
		{
			WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(60))
			{
				PollingInterval = TimeSpan.FromMilliseconds(300)
			};

			wait.Until(x => string.Compare(x.Url, url, StringComparison.InvariantCultureIgnoreCase) == 0);
			RaiseNavigated(_driver.Url);
			_driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
		}
	}
}
