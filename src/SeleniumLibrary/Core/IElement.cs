﻿using System;
using SeleniumLibrary.Core;

namespace SeleniumLibrary
{
	public interface IElement : IElementFinder
	{
		string GetAttribute(string name);

		void WaitForExists();

		void WaitForNotExists();

		void Click();

		void MouseClick();

		bool IsVisible { get; }

		int Width { get; }

		string CssClass { get; }

		string Content { get; }
	}
}
