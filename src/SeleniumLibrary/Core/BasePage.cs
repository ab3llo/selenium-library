﻿using System;
using OpenQA.Selenium;

namespace SeleniumLibrary.Core
{
	public abstract class BasePage
	{
        protected SeleniumDriver _seleniumDriver;
		private IWebDriver _driver;
		public BasePage(SeleniumDriver seleniumDriver)
		{
			_seleniumDriver = seleniumDriver;
			_driver = seleniumDriver.GetBrowser();
		}

		public BasePage()
		{

		}
	}
}
