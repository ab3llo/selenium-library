﻿namespace SeleniumLibrary.Enums
{
	public enum Browsers
	{
		Chrome,
		Firefox,
		IE11,
		Safari,
		Opera
	}
}