﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace SeleniumLibrary.Config
{
    public class ConfigurationHelper
    {
        private ConfigurationHelper()
        {
            
        }

        public static string Get(string key)
        {
            IConfigurationRoot configuration = ReadConfiguration();
            return configuration.GetSection(key).Value;
        }

        public static IConfigurationRoot ReadConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appSettings.json");

            return builder.Build();
        }
	}
}
