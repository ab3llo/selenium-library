**Selenium Library**

This is a selenium wrapper library, written in C# intended to run using selenium remote driver. 
It is currently running against the selenium grid in the docker container that is composed using the docker-compose file that can be found in **selenium-library/src/tests/SeleniumLibrary.Tests/**

 =====================================================================
 
**Set Up Selenium Grid**

First it requires that you have installed docker and the docker daemon is up and running.

**Docker**

Navigate to the **selenium-library/src/tests/SeleniumLibrary.Tests/** and run the docker command

**docker-compose up**

This command will use the **docker-compose.yml** file to pull the latest images for the selenium-hub, selenium-chrome-debug-node and selenium-firefox-debug-node and create a local selenium grid server for you to run your tests against.

The selenium-chrome-debug-nodes and selenium-firefox-debug-nodes create vnc severs that can be access to view the running tests


**VNC Viewer (ONLY NEEDED for DOCKER)**

Download VNC Viewer from https://www.realvnc.com/en/connect/download/viewer/

Add the following connections to your vnc viewer 

  - localhost:5900 for chrome node 
  
  - localhost:5901 for firefox
 
 
**Or Selenium Standalone**

Download Selenium Standalone Server
https://www.seleniumhq.org/download/

And follow the step by step guid in this blog

https://www.joecolantonio.com/selenium-grid-how-to-setup-a-hub-and-node/

 =====================================================================
 
 **Running the Tests Project**
 
 Navigate to the test project directory "**selenium-library/src/tests/SeleniumLibrary.Tests/**"
 
 And run the **dotnet test**  command 
 
 This will kick of a test run via the command line
 
 Or open up visual studion and in the test explorer select the test you want to run
 
